import React, {useEffect} from 'react'
import { ImageBackground, StyleSheet, Text, View } from 'react-native'

const splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('App')
        }, 3000)
    }, [navigation])
    return (
        <ImageBackground source={require('./src/assets/splash.png')} style={styles.background}>

        </ImageBackground>
    )
}

export default splash

const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
