import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, ImageBackground } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeActive from './src/assets/svg/active-home.svg';
import HomeInactive from './src/assets/svg/inactive-home.svg';
import PesananActive from './src/assets/svg/active-pesanan.svg';
import PesananInactive from './src/assets/svg/inactive-pesanan.svg';
import AkunActive from './src/assets/svg/active-akun.svg';
import AkunInactive from './src/assets/svg/inactive-akun.svg';
import Timbangan from './src/assets/svg/041-scale.svg';
import Satuan from './src/assets/svg/009-clean clothes.svg';
import Vip from './src/assets/svg/010-dress.svg';
import Karpet from './src/assets/svg/011-laundry.svg';
import Setrika from './src/assets/svg/021-iron.svg';
import Ekspress from './src/assets/svg/036-delivery truck.svg';
import Mesin from './src/assets/svg/001-washing machine.svg';
import Saldo from './src/assets/svg/005-measuring spoon.svg';
import Point from './src/assets/svg/043-laundry.svg';
import Logo from './src/assets/svg/logo.svg';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const Tab = createBottomTabNavigator();

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      timePassed: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setTimePassed();
    }, 1000);
  }

  setTimePassed = () => {
    this.setState({
      timePassed: true
    });
  }

  HomeScreen = () => {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerLeft}>
            <View style={styles.logo}>
              <Image
                style={styles.tinyLogo}
                source={require('./src/assets/svg/logo.png')}
                />
            </View>
            <View style={styles.nama}>
              <Text style={{fontSize: 20, fontFamily: 'Roboto'}}>Selamat Datang,</Text>
              <Text style={{fontSize: 20, fontFamily: 'Roboto', fontWeight: 'bold'}}>Agung</Text>
            </View>
          </View>
          <View style={styles.headerRight}>
            <Image
            style={styles.logoKanan}
            source={require('./src/assets/svg/courier.png')}
            />
          </View>
        </View>
        <View style={styles.layananContainer}>
          <View style={styles.cardContainer}>
            <View style={styles.card}>
              <View style={{ flex: 1, flexDirection: 'column' }}>
                <Text>Saldo:</Text>
                <Text style={{fontSize: 10}}>Antar Point:</Text>
              </View>
              <View style={{ flex: 1.2 }}>
                <Text style={{fontWeight: 'bold'}}>Rp. 250.000</Text>
                <Text style={{fontSize: 10, color: '#55CB95', textAlign: 'right'}}>200 point</Text>
              </View>
              <View style={{ flex: 1}}>
                <View style={styles.iconHeaderContainer}>
                  <View style={styles.iconHeader}>
                    <Saldo width={'100%'} height={'100%'}></Saldo>
                  </View>
                  <Text style={{fontSize: 9}}>Add Saldo</Text>
                </View>
              </View>
              <View style={{ flex: 1 }}>
                <View style={styles.iconHeaderContainer, {marginLeft: 25}}>
                  <View style={styles.iconHeader}>
                    <Point width={'100%'} height={'100%'}></Point>
                  </View>
                  <Text style={{fontSize: 9}}>Get Points</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.layanan}>
            <Text style={{ fontWeight: 'bold',marginBottom:-15 }}>Layanan Kami</Text>
            <Text style={{ fontWeight: 'bold', borderBottomColor: '#55CB95', borderBottomWidth: 2, width: 55 }}></Text>
            <View style={styles.iconLayananContainer}>
              <View style={styles.iconLayananSubContainer}>
                <View style={styles.iconLayanan}>
                  <Timbangan width={'100%'} height={'100%'}></Timbangan>
                </View>
                <Text style={styles.fontIcon}>Kiloan</Text>
              </View>
              <View style={styles.iconLayananSubContainer}>
                <View style={styles.iconLayanan}>
                  <Satuan width={'100%'} height={'100%'}></Satuan>
                </View>
                <Text style={styles.fontIcon}>Satuan</Text>
              </View>
              <View style={styles.iconLayananSubContainer}>
                <View style={styles.iconLayanan}>
                  <Vip width={'100%'} height={'100%'}></Vip>
                </View>
                <Text style={styles.fontIcon}>VIP</Text>
              </View>
              <View style={styles.iconLayananSubContainer}>
                <View style={styles.iconLayanan}>
                  <Karpet width={'100%'} height={'100%'}></Karpet>
                </View>
                <Text style={styles.fontIcon}>Karpet</Text>
              </View>
              <View style={styles.iconLayananSubContainer}>
                <View style={styles.iconLayanan}>
                  <Setrika width={'100%'} height={'100%'}></Setrika>
                </View>
                <Text style={styles.fontIcon}>Setrika Saja</Text>
              </View>
              <View style={styles.iconLayananSubContainer}>
                <View style={styles.iconLayanan}>
                  <Ekspress width={'100%'} height={'100%'}></Ekspress>
                </View>
                <Text style={styles.fontIcon}>Ekspress</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.pesananContainer}>
          <View style={{paddingLeft: 50, paddingRight: 50,}}> 
            <Text style={{ fontWeight: 'bold',marginBottom:-15 }}>Pesanan Aktif</Text>
            <Text style={{ fontWeight: 'bold', borderBottomColor: '#55CB95', borderBottomWidth: 2, width: 55 }}></Text>
          </View>
          <View style={{paddingLeft: 20, paddingRight: 20,marginTop: 5, paddingBottom: 40}}> 
            <ScrollView>
              <View style={styles.pesanan}>
                <View style={styles.cardPesanan}>
                  <View style={{flex: 0.2, alignItems: 'flex-start', justifyContent: 'flex-start'}}><Mesin width={'100%'} height={'100%'}></Mesin></View>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text style={{fontWeight: 'bold'}}>Pesanan No. 0002142</Text>
                    <Text style={{fontSize: 10, color: '#55CB95'}}>Sudah selesai</Text>
                  </View>
                </View>
                <View style={styles.cardPesanan}>
                  <View style={{flex: 0.2, alignItems: 'flex-start', justifyContent: 'flex-start'}}><Mesin width={'100%'} height={'100%'}></Mesin></View>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text style={{fontWeight: 'bold'}}>Pesanan No. 0003584</Text>
                    <Text style={{fontSize: 10, color: 'red'}}>Belum selesai</Text>
                  </View>
                </View>
                <View style={styles.cardPesanan}>
                  <View style={{flex: 0.2, alignItems: 'flex-start', justifyContent: 'flex-start'}}><Mesin width={'100%'} height={'100%'}></Mesin></View>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text style={{fontWeight: 'bold'}}>Pesanan No. 0005862</Text>
                    <Text style={{fontSize: 10, color: '#55CB95'}}>Sudah selesai</Text>
                  </View>
                </View>
                <View style={styles.cardPesanan}>
                  <View style={{flex: 0.2, alignItems: 'flex-start', justifyContent: 'flex-start'}}><Mesin width={'100%'} height={'100%'}></Mesin></View>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text style={{fontWeight: 'bold'}}>Pesanan No. 0008562</Text>
                    <Text style={{fontSize: 10, color: 'red'}}>Belum selesai</Text>
                  </View>
                </View>
                <View style={styles.cardPesanan}>
                  <View style={{flex: 0.2, alignItems: 'flex-start', justifyContent: 'flex-start'}}><Mesin width={'100%'} height={'100%'}></Mesin></View>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text style={{fontWeight: 'bold'}}>Pesanan No. 0007546</Text>
                    <Text style={{fontSize: 10, color: 'red'}}>Belum selesai</Text>
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }

  SettingsScreen = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
  
  ProfileScreen = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Profile</Text>
      </View>
    );
  }

  render() {
    return (
      this.state.timePassed === false ?
      <ImageBackground source={require('./src/assets/splash.png')} style={styles.background}>

      </ImageBackground>
      :
        <NavigationContainer>
          <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused}) => {
                if (route.name === 'Home') {
                  return focused ? <HomeActive style={{marginBottom: -15, marginLeft: -9, textAlign: 'center'}} width={40} height={40}></HomeActive> : <HomeInactive style={{marginBottom: -15, marginLeft: -9, textAlign: 'center'}} width={40} height={40}></HomeInactive>;
                } else if (route.name === 'Pesanan') {
                  return focused ? <PesananActive style={{marginBottom: -10, marginLeft: -7, textAlign: 'center'}} width={40} height={40}></PesananActive> : <PesananInactive style={{marginBottom: -10, marginLeft: -7, textAlign: 'center'}} width={40} height={40}></PesananInactive>;
                } else if (route.name === 'Akun') {
                  return focused ? <AkunActive style={{marginBottom: -15, marginLeft: -2, textAlign: 'center'}} width={40} height={40}></AkunActive> : <AkunInactive style={{marginBottom: -15, marginLeft: -2, textAlign: 'center'}} width={40} height={40}></AkunInactive>
                }
              },
            })}
            tabBarOptions={{
              activeTintColor: '#55CB95',
              inactiveTintColor: 'gray',
              style: {
                width: '100%',
                borderTopRightRadius: 10,
                borderTopLeftRadius: 10,
                padding: 0,
                textAlign: 'center',

              }
            }}
          >
            <Tab.Screen name="Home" component={this.HomeScreen} />
            <Tab.Screen name="Pesanan" component={this.SettingsScreen} />
            <Tab.Screen name="Akun" component={this.ProfileScreen} />
          </Tab.Navigator>
          </NavigationContainer>
    );
  }

}

export default App

const styles = StyleSheet.create({
  svgContainer: {
    color: '#55CB95',
  },
  tabScreen: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 10
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFFF'
  },
  header: {
    paddingLeft: 30,
    paddingTop: 10,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E0F7EF',
    borderBottomRightRadius: 60,
    borderBottomLeftRadius: 60,
    
  },
  layananContainer: {
    flex: 2,
    backgroundColor: '#FFFF',
  },
  pesananContainer: {
    flex: 1,
    backgroundColor: '#F6F6F6',
    borderTopColor: '#55CB95',
    borderRightColor: '#55CB95',
    borderLeftColor: '#55CB95',
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    marginRight: -1,
    marginLeft: -1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingTop: 10
  },
  headerLeft: {
    flex: 1,
    flexDirection: 'column'
  },
  headerRight: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 10
  },
  logo: {
    flex: 0.50
  },
  nama: {
    flex: 1,
    flexDirection: 'column'
  },
  tinyLogo: {
    width: 80,
    height: 39,
  },
  logoKanan: {
    width: 150,
    height: 166,
    borderBottomRightRadius: 40
  },
  cardContainer: {
    marginTop: -40,
    paddingLeft: 30,
    paddingRight: 30,
    flex: 0.5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  card: {
    flex: 0.55,
    flexDirection: 'row',
    borderRadius: 10,
    width: '100%',
    height: '100%',
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 15,
    paddingRight: 15,
    backgroundColor: '#FFFF',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6,
  },
  layanan: {
    flex: 1,
    paddingLeft: 50,
    paddingRight: 50,
    marginTop: -20
  },
  iconLayananContainer: {
    marginTop: 10,
    flex: 1,
    backgroundColor: '#FFFF',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  iconLayanan: {
    width: '100%',
    height: '100%',
    padding: 10,
    borderRadius: 10,
    backgroundColor: '#E0F7EF'
  },
  iconLayananSubContainer: {
    width: 70,
    height: 70,
    borderRadius: 10,
    backgroundColor: '#FFFF',
    alignItems: 'center',
    marginBottom: 40
  },
  fontIcon: {
    fontSize: 10,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#565656'
  },
  pesanan: {
    flex: 1,
    flexDirection: 'column',
  },
  cardPesanan: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    margin: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#FFFF',
    borderRadius: 2,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  iconHeaderContainer: {
    width: 60,
    height: 40,
    borderRadius: 5,
    backgroundColor: '#FFFF',
    alignItems: 'center',
    marginLeft: 25
  },
  iconHeader: {
    width: '100%',
    height: '100%',
    width: 40,
    height: 40,
    padding: 5,
    borderRadius: 5,
    backgroundColor: '#E0F7EF'
  },
  background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})